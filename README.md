# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Edvard Bjørklund Eide"  
STUDENT ID = "10050"

## Project description

[//]: # This project was to develop a system that can save information about train departures. The user of this system will be able to edit, add, delete and sort these traindepartures. The train departures will have the attributes departure time, line, destination, delay, track and a unique train number. The specific functional demands are as follows: The user will be able to 
see all the train departures sorted by departure time, add train departures (train numbers can not be duplicates), edit track and delay for existing train departures and update a system clock that will delete train departures that have left. In addition the user will be able to search for train departures either by train number or destination, and eventually the user will be able to quit the program.
## Project structure

[//]: # The source files are stored in the src folder, divided in main files and test files. In the main file you will find four classes, the train departure class, register class, ui class and a main class. The train departure file holds the class that has all the attributes for each train departure as well as setters and getters for them. The register file you will find the class that holds each train departure as well as methods to add, delete, edit and sort the departures. In the ui file you will find the class that holds the user interface, this is what the user will see and be able to interact with. Lastly the main file has the main method used to initialize and start the ui so that the user can interact with the program. In the test file you will find JUnit-test classes for each corresponding class in the main folder.

## Link to repository

[//]: # https://gitlab.stud.idi.ntnu.no/edvardei/programmering-1-train

## How to run the project

[//]: #  To run the project you have to run the main method in the main file, here there are two methods. The first method init() is used to initialize the register object, so that the start() method can use it for the ui. In the start() method the user will firstly see all train departures already added, and then it will see numerated lines 0-8 with choices to do. For example input "1" will let the user add a train departure, as long as the input is in correct format and the train number is unique, this will be added. Another example would be to set a delay for a train departure, the user puts in "3" and then has to write in an already existing train number and then the user will be able to set a new delay (In HH:MM format) for this train departure. If the input is in wrong format or otherwise illegal the program should not crash, but rather let the user try again after giving the user a message of what went wrong.

## How to run the tests

[//]: #To run the tests the user will have to go to each of the test classes and run them individually. When you run the overarching testclass for each of the test files both the positive and the negative test will run. 

## References

[//]: # See references in the corresponding report.
