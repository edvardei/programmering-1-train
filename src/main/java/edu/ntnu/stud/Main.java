package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 * The main function is to print the UserInterface and let the user interact with  it.
 *goal : act as the main class for the train dispatch application.
 *
 * @author 10050.
 * @version 1.0
 * @since 0.3
 */
public class Main {
  /**
   * The main function for the train dispatch application.
   *
   * @param args The arguments for the main function.
   * @throws IllegalArgumentException if the user input is invalid.
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();
    ui.init();
    ui.start();
  }
}
