package edu.ntnu.stud;

import java.util.Scanner;

/**
 * this is the user interface for the train departure application.
 * It has two methods, init and start.
 * init is used to initialize the train departure register.
 * start is used to start the user interface.
 * goal : act as a user interface for the train departure application.
 *
 * @author 10050.
 * @version 1.0
 * @since 0.3
 */
public class UserInterface {

  private static final String QUIT_PROGRAM = "0";
  private static final String ADD_TRAIN = "1";
  private static final String DELETE_TRAIN = "2";
  private static final String SET_DELAY = "3";
  private static final String SET_TRACK = "4";
  private static final String SEARCH_NUMBER = "5";
  private static final String SEARCH_DESTINATION = "6";
  private static final String UPDATE_LIST = "7";
  private static final String SEE_INTERVAL = "8";
  private static final String SEE_LIST = "9";

  private TrainDepartureRegister trainDepartureRegister;

  /**
   * Verifies that the user input is in a valid format and is a valid choice in the user interface.
   * The user input has to be a number between 0 and 8.
   *
   * @param choice is a String that is parsed to an integer to check if it is a valid choice.
   *               for example if the user inputs 0, the program will quit.
   * @throws IllegalArgumentException if the user input is negative or above 8.
   */
  public void verifyChoice(String choice) throws IllegalArgumentException {
    try {
      Integer.parseInt(choice);
      if (Integer.parseInt(choice) < 0 || Integer.parseInt(choice) > 9) {
        throw new IllegalArgumentException("Invalid input, please try again");
      }
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Invalid input, please try again");
    }
  }


  /**
   * Initializes the train departure register.
   */
  public void init() {
    trainDepartureRegister = new TrainDepartureRegister();
  }

  /**
   * Starts the user interface.
   */
  public void start() {
    trainDepartureRegister.addTrainDeparture("10:00", "L1", "62", "Bodø", "00:00");
    trainDepartureRegister.addTrainDeparture("12:00", "L2", "63", "Trondheim", "00:00");
    trainDepartureRegister.addTrainDeparture("13:00", "L3", "64", "Oslo", "00:00");
    trainDepartureRegister.addTrainDeparture("14:00", "L4", "65", "Bergen", "00:00");
    trainDepartureRegister.setDelayForTrainDeparture("62", "00:30");
    trainDepartureRegister.setTrackForTrainDeparture("62", "2");
    trainDepartureRegister.setTrackForTrainDeparture("63", "1");
    trainDepartureRegister.setDelayForTrainDeparture("64", "00:30");
    System.out.println("Welcome to the train departure application");
    Scanner scanner = new Scanner(System.in);
    boolean bol = true;
    trainDepartureRegister.showAllTrainDepartures();
    while (bol) {
      try {
        System.out.println("0. Quit program");
        System.out.println("1. Register a new train departure.");
        System.out.println("2. Delete a trainDeparture");
        System.out.println("3. Set a delay on a train departure.");
        System.out.println("4. Set track on a train departure.");
        System.out.println("5. Search for a train departure using train number.");
        System.out.println("6. Search for all train departures to a destination.");
        System.out.println("7. Update the train departure list.");
        System.out.println("8. See train departures in an interval.");
        System.out.println("9. See the train departure list");
        String choice = scanner.next();
        verifyChoice(choice);
        switch (choice) {
          case QUIT_PROGRAM -> {
            bol = false;
            System.out.println("The program is now closed.");
          }
          case ADD_TRAIN -> {
            try {
              System.out.println("Write a departure time");
              String departureTime = scanner.next();
              System.out.println("Write a line");
              String line = scanner.next();
              System.out.println("Write a train number(has to be unique)");
              String trainNumber = scanner.next();
              System.out.println("Write a destination");
              String destination = scanner.next();
              System.out.println("Write a delay (can be 00:00)");
              String delay = scanner.next();
              trainDepartureRegister.addTrainDeparture(departureTime, line, trainNumber,
                  destination,
                  delay);
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case DELETE_TRAIN -> {
            try {
              System.out.println("Write a train Number for the train departure you want to delete");
              String trainNumber = scanner.next();
              trainDepartureRegister.deleteTrainDeparture(trainNumber);
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SET_DELAY -> {
            try {
              System.out.println("Write a train number for the train departure you want to delay");
              String trainNumber = scanner.next();
              System.out.println("Write a delay (HH:MM)");
              String delay = scanner.next();
              trainDepartureRegister.setDelayForTrainDeparture(trainNumber, delay);
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SET_TRACK -> {
            try {
              System.out.println("Write a train number for the train you want to set a track for");
              String trainNumber = scanner.next();
              System.out.println("Write a track (Has to be between 1 and 6)");
              String track = scanner.next();
              trainDepartureRegister.setTrackForTrainDeparture(trainNumber, track);
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SEARCH_NUMBER -> {
            try {
              System.out.println("Write a train number for the train you want to search for");
              String trainNumber = scanner.next();
              System.out.println(
                  trainDepartureRegister.findTrainDepartureUsingTrainNumber(trainNumber));
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SEARCH_DESTINATION -> {
            try {
              System.out.println("Write a destination you want to search for");
              String destination = scanner.next();
              System.out.println(
                  trainDepartureRegister.findAllDeparturesToDestination(destination));
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case UPDATE_LIST -> {
            try {
              System.out.println("Write a station clock time (HH:MM)");
              String stationClock = scanner.next();
              trainDepartureRegister.setStationClock(stationClock);
              trainDepartureRegister.updateTrainDepartures();
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SEE_INTERVAL -> {
            try {
              System.out.println("Write a start time for interval (HH:MM)");
              String startTime = scanner.next();
              System.out.println("Write an end time for interval (HH:MM)");
              String endTime = scanner.next();
              System.out.println(trainDepartureRegister
                  .findTrainDeparturesInInterval(startTime, endTime));
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          case SEE_LIST -> {
            try {
              System.out.println(trainDepartureRegister.showAllTrainDepartures());
            } catch (IllegalArgumentException e) {
              System.out.println(e.getMessage());
            }
          }
          default -> throw new IllegalArgumentException("Invalid input, please try again");
        }
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }
    }
  }
}
