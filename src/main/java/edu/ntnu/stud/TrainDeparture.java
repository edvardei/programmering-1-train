package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * .
 * TrainDeparture is a class that represents a train departure.
 * It contains information about departure time, line, train number, destination, track and delay.
 * The class has two constructors, one if track is unknown and one if track is known.
 * The class also has methods to add delay to departure time and to set track.
 * The class also has a method to verify input from user, both for numbers and strings.
 * goal : act as a train departure object.
 *
 * @author 10050.
 * @version 1.0
 * @since 0.1
 */

public class TrainDeparture {
  //departureTime, line, trainNumber and destination are all final because the user will not be
  //able to change these attributes.
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  //chose track and delay not to be final because it can be changed by the user.
  private int track;
  private LocalTime delay;

  /**
   * .
   * Constructs a train departure if the track is unknown, and all attributes become parameters
   * for each train departure. Does this only after verifying that the input from user is valid.
   * by performing the method:
   * :{@link #verifyParameter(String, String)}
   *
   * @param departureTime Is a string that gets parsed in the constructor to the LocalTime class.
   *                      Representing the departure time for each train departure.
   * @param line          Represents a combination of letters and integers describing what line
   *                      the train departure is on.
   * @param trainNumber   Is a string that is parsed in the constructor to the Integer class, this
   *                      Integer is a unique number for each train departure.
   * @param destination   Is a String representing where the train departure stops.
   * @param delay         Is a string that gets parsed the constructor to the localTime class.
   *                      Representing the delay for each train departure. this can be 00:00.
   *
   * @throws IllegalArgumentException Throws input that is invalid such as blank strings and
   *                                  negative numbers.
   */
  public TrainDeparture(String departureTime, String line, String trainNumber, String destination,
                        String delay) throws IllegalArgumentException {
    verifyParameter(departureTime, "departure time");
    verifyParameter(line, "line");
    verifyParameter(trainNumber, "train number");
    verifyParameter(destination, "destination");
    verifyParameter(delay, "delay");
    this.departureTime = LocalTime.parse(departureTime);
    this.line = line;
    this.trainNumber = Integer.parseInt(trainNumber);
    this.destination = destination;
    this.track = -1;
    this.delay = LocalTime.parse(delay);
  }

  /**
   * .
   * Constructor if track is known. uses the constructor above and adds track as a parameter.
   * Does this only after verifying that the input from user is valid. by performing the method:
   * :{@link #verifyParameter(String, String)}
   *
   * @param departureTime Is a string that gets parsed in the constructor to the LocalTime class.
   *                      Representing the departure time for each train departure.
   * @param line          Represents a combination of letters and integers describing what line
   *                      the train departure is on.
   * @param trainNumber   Is a string that is parsed in the constructor to the Integer class, this
   *                      Integer is a unique number for each train departure.
   * @param destination   Is a String representing where the train departure stops.
   * @param track         is a String that is parsed in the constructor to the Integer class, this
   *                      number represents what physical track the train is currently on.
   * @param delay         Is a string that gets parsed the constructor to the localTime class.
   *                      Representing the delay for each train departure. this can be 00:00.
   *
   * @throws IllegalArgumentException Throws input that is invalid such as if the track is 0.
   */
  public TrainDeparture(String departureTime, String line, String trainNumber, String destination,
                        String track, String delay) throws IllegalArgumentException {

    this(departureTime, line, trainNumber, destination, delay);
    verifyParameter(track, "track");
    this.track = Integer.parseInt(track);
  }
  //------------Getters-----------------
  /**
   * returns departure time for the train departure.
   *
   * @return a Departure time in the form of a LocalTime object.
   */

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * returns the line for the train departure.
   *
   * @return a String in with a combination of letters and integers.
   */

  public String getLine() {
    return line;
  }

  /**
   * returns the unique train number for the train departure.
   *
   * @return an integer of the train number.
   */

  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * returns a string representing the destination for the train departure.
   *
   * @return a string representing the destination.
   */

  public String getDestination() {
    return destination;
  }

  /**
   * returns an integer that represents which track the train is on.
   *
   * @return an integer representing the track.
   */

  public int getTrack() {
    return track;
  }

  /**
   * returns a delay time for the train departure.
   *
   * @return a delay time in the form of a LocalTime object.
   */

  public LocalTime getDelay() {
    return delay;
  }

  //------------Setters-----------------

  /**
   * .
   * Takes a String and parses it to set a new delay for an existing train departure.
   * Does this only after verifying that the input from user is valid.
   * this allows for editing of the delay for a train departure.
   *
   * @param newDelay is a string that gets parsed to the LocalTime class.
   *
   * @throws IllegalArgumentException Throws input that is invalid such as blank strings.
   */
  public void setDelay(String newDelay) {
    verifyParameter(newDelay, "delay");
    this.delay = LocalTime.parse(newDelay);
  }

  /**
   * Sets an integer for what track an existing train object is on.
   * This allows for editing of the track for a train departure.
   *
   * @param newTrack is an integer for what track is to be used.
   *
   * @throws IllegalArgumentException Throws input that is invalid such as negative numbers and 0.
   */
  public void setTrack(int newTrack) {
    this.track = newTrack;
  }

  //------------Verification method-----------------

  /**
   * .
   * Verifies if a given String is containing information, if the String is blank, the check fails.
   *
   * @param parameter     Is the String that's being verified.
   * @param parameterName Is the name of the String that's being verified, this allows for a
   *                      specific error message to be printed.
   *
   * @throws IllegalArgumentException Throws input that is blank, for example "", then it is
   *                                  considered illegal because not enough information is given.
   */
  public void verifyParameter(String parameter, String parameterName) {
    if (parameter.isBlank()) {
      throw new IllegalArgumentException(parameterName + " cannot be blank");
    }
  }

  /**
   * .
   * Adds delay to departure time for a train departure. Uses .plusHours and .plusMinutes from
   * LocalTime class to add delay to departure time. This allows for a train departure to be
   * sorted by departure time plus delay. So that the user gets a more accurate timetable.
   *
   *
   * @return newDeparture is the new departure time for the train departure. with delay added.
   */
  public LocalTime addDelayToDepartureTime() {
    //used primarily for the sorting method in TrainDepartureRegister.
    return getDepartureTime().plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  //------------toString-----------------

  /**
   * prints a string for a train departure with all of its attributes.
   *
   * @return Prints out the information about the train departure, in the form of a timetable.
   */
  //used for printing out the timetable in the user interface.
  //used copilot to help with the string formatting.
  @Override
  public String toString() {
    StringBuilder timetable = new StringBuilder();
    timetable.append("+--------------+---------------------+\n");
    timetable.append(String.format("| %-12s", "Departure:")).append("| ").append(getDepartureTime())
        .append("\n");
    timetable.append(String.format("| %-12s", "Line:")).append("| ").append(getLine()).append("\n");
    timetable.append(String.format("| %-12s", "Train:")).append("| ").append(getTrainNumber())
        .append("\n");
    timetable.append(String.format("| %-12s", "Destination:")).append("| ").append(getDestination())
        .append("\n");
    if (track != -1) {
      timetable.append(String.format("| %-12s", "Track:")).append("| ").append(getTrack())
          .append("\n");
    }
    if (!delay.equals(LocalTime.parse("00:00"))) {
      timetable.append(String.format("| %-12s", "Delay:")).append("| ").append(getDelay())
          .append("\n");
    }
    timetable.append(String.format("| %-12s", "Estimated time:")).append("| ")
        .append(addDelayToDepartureTime()).append("\n");
    timetable.append("+--------------+---------------------+\n");
    return timetable.toString();
  }
}
