package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * TrainDepartureRegister is a class that represents a register of all the train departures.
 * It contains a list of train departures and a station clock.
 * The station clock is used to update the list of train departures.
 * Through the list of train departures the user can use different methods to find specific
 * train departures, add new train departures and update the list of train departures, in addition
 * the user can sort the list of train departures.
 * goal : act as a timetable of train departures.
 *
 * @author 10050.
 * @version 1.0
 * @since 0.2
 */
public class TrainDepartureRegister {

  ArrayList<TrainDeparture> trainDepartures = new ArrayList<>();

  private LocalTime stationClock;

  /**
   * Constructs a new TrainDepartureRegister, with a default station clock of 05:00.
   * the station clock is used to update the list of train departures.
   */

  public TrainDepartureRegister() {
    //default station clock is 05:00
    this.stationClock = LocalTime.parse("05:00");
  }

  //-----------Getters and setters-----------------

  /**
   * returns the station clock and makes it a string that is readable for the user.
   *
   * @return a String in the hh:mm format that represents the station clock.
   */

  public String getStationClock() {
    return stationClock.toString();
  }

  /**
   * returns the number of unique train departures in the array list.
   *
   * @return an Integer representing how many unique train departures there are.
   */

  public int getSize() {
    return trainDepartures.size();
  }


  /**
   * returns the train departure at the specified index in the array list. This method is used
   * mostly for testing purposes.
   *
   * @param index is an integer that represents the index of the train departure in the array list.
   * @return a train departure at the specified index.
   */
  public TrainDeparture getTrainDeparture(int index) {
    return trainDepartures.get(index);
  }

  /**
   * Sets the station clock to the desired time of the user. does this only after verifying that
   * the input from user is in the correct format of the LocalTime class and parses it.
   * verifies the input by performing the method:
   * :{@link #verifyLocalTimeFormat(String, String)}
   *
   * @param newStationClock is the new time for the Station clock
   * @throws IllegalArgumentException Throws input that is blank or not in the correct format.
   *                                  also throws input if the new station clock is before the
   *                                  current station clock.
   */

  public void setStationClock(String newStationClock) throws IllegalArgumentException {
    //checks if the input is blank
    if (newStationClock.isBlank()) {
      throw new IllegalArgumentException("Station clock cannot be blank");
    }
    //then checks if the input is in the correct format
    verifyLocalTimeFormat(newStationClock, "station clock");
    //then checks if the new station clock is before the current station clock
    if (LocalTime.parse(newStationClock).isBefore(stationClock)) {
      throw new IllegalArgumentException("Station clock cannot be before: " + stationClock);
    }
    this.stationClock = LocalTime.parse(newStationClock);
  }
  //-----------Methods-----------------

  /**
   * Adds a train departure to the already existing list of train departures, using all the
   * required parameters used to define a single train departure.This method utilizes
   * the constructor of the TrainDeparture class that has an unknown track. only adds after
   * verifying that the String inputs from user are in the correct format. and that the train
   * departure does not already exist in the list of train departures using the methods:
   * :{@link #verifyLocalTimeFormat(String, String)}
   * :{@link #verifyIntegerFormat(String, String)}
   *
   * @param departureTime     Is a string that gets parsed in the constructor to the LocalTime
   *                          class Representing the departure time for each train departure.
   * @param line              Represents a combination of letters and integers describing what line
   *                          the train departure is on.
   * @param trainNumberString is a String that is parsed to an Integer in the method and
   *                          then is compared to the train numbers of the already existing.
   * @param destination       Is a String representing where the train departure stops.
   * @param delay             Is a string that gets parsed the constructor to the localTime class.
   *                          Representing the delay for each train departure. this can be 00:00.
   * @throws IllegalArgumentException Throws input that is blank or not in the correct format.
   *                                  Also throws input if a train departure already exists in
   *                                  the list of existing train departures.
   */
  public void addTrainDeparture(String departureTime, String line, String trainNumberString,
                                String destination, String delay) throws IllegalArgumentException {
    //verifies that the input is in the correct format
    verifyLocalTimeFormat(departureTime, "Departure time");
    verifyLocalTimeFormat(delay, "delay");
    verifyIntegerFormat(trainNumberString, "Train number");
    //have to parse it to an int to compare it to the train numbers of the already existing
    int trainNumber = Integer.parseInt(trainNumberString);
    //checks if the train departure already exists in the list of train departures
    trainDepartures.stream().filter(
            trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst()
        //if it does exist it throws an exception
        .ifPresent(trainDeparture -> {
          throw new
              IllegalArgumentException("The train departure you are trying to add already exists");
        });
    //if it does not exist it adds it to the list of train departures
    trainDepartures.add(
        new TrainDeparture(departureTime, line, trainNumberString, destination, delay));
  }

  /**
   * Deletes a train departure from the already existing list of train departures. does this
   * only after verifying the user input and that the train departure exists in the list of
   * train departures. if the train departure does not exist it throws an exception.
   * verifies user input using the method:
   * :{@link #verifyIntegerFormat(String, String)}
   *
   * @param trainNumberString is a String that is parsed to an Integer in the method and is the
   *                          specific train number for the train departure the user wants to
   *                          delete.
   * @throws IllegalArgumentException if the train number does not have a corresponding
   *                                  train departure.
   */
  public void deleteTrainDeparture(String trainNumberString) {
    //verifies that the input is in the correct format
    verifyIntegerFormat(trainNumberString, "Train number");
    int trainNumber = Integer.parseInt(trainNumberString);
    //checks if the train departure exists in the list of train departures
    if (trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst()
        //if it does not exist it throws an exception
        .isEmpty()) {
      throw new
          IllegalArgumentException("The train departure you are trying to delete does not exist");
    } else {
      //if it does exist it deletes it from the list of train departures
      trainDepartures.removeIf(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber));
    }
  }

  /**
   * Uses streams to set a new delay for an already existing train departure. if the train number
   * input does not have a corresponding train departure it throws an exception.
   * also verifies that the delay is in the correct format. using the methods:
   * :{@link #verifyLocalTimeFormat(String, String)}
   * :{@link #verifyIntegerFormat(String, String)}
   *
   * @param trainNumberString is a String that is parsed to an Integer in the method and is the
   *                          specific train number for the train departure the user searches for.
   * @param delay             Is a string that gets parsed the constructor to the localTime class.
   *                          and is the new delay for the train departure.
   * @throws IllegalArgumentException if the train number does not have a corresponding
   *                                  train departure.
   */
  public void setDelayForTrainDeparture(String trainNumberString, String delay) {
    //verifies that the input is in the correct format
    verifyLocalTimeFormat(delay, "delay");
    verifyIntegerFormat(trainNumberString, "train number");
    int trainNumber = Integer.parseInt(trainNumberString);
    //checks if the train departure exists in the list of train departures
    trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst()
        //if it exists it sets the new delay for the existing train departure
        .ifPresent(trainDeparture -> trainDeparture.setDelay(delay));
    //if it does not exist it throws an exception
    if (trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst().isEmpty()) {
      throw new IllegalArgumentException("The train departure does not exist");
    }
  }

  /**
   * Uses streams to set a new track for an already existing train departure. does this only
   * after verifying the input from the user. if the track is above 6 and if the train number
   * does not have a corresponding train departure in the list it throws an exception. also
   * throws an exception if the track is already occupied.
   * verifies user input using the methods:
   * :{@link #verifyIntegerFormat(String, String)}
   * :{@link #verifyIntegerFormat(String, String)}
   *
   * @param trainNumberString is a String that is parsed to an Integer in the method and represents
   *                          the specific train departure the user wants to set a new track for.
   * @param trackString       Is a string that gets parsed to the Integer class in the method and
   *                          represents the new track for the train departure. can not be above 6,
   *                          as there are only 6 tracks and can not be occupied by another
   *                          train departure.
   * @throws IllegalArgumentException if the train number does not have an existing train departure
   *                                  and if the track is above 6 or if the track is already
   *                                  occupied.
   */

  public void setTrackForTrainDeparture(String trainNumberString, String trackString) {
    //verifies that the input is in the correct format
    verifyIntegerFormat(trainNumberString, "train number");
    verifyIntegerFormat(trackString, "track");
    int trainNumber = Integer.parseInt(trainNumberString);
    int track = Integer.parseInt(trackString);
    //checks if the track is above 6 if it is it throws an exception
    if (track > 6) {
      throw new IllegalArgumentException("There are only 6 tracks");
    }
    //checks if the track is already occupied if it is it throws an exception
    trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrack() == (track))
        .findFirst()
        .ifPresent(trainDeparture -> {
          throw new IllegalArgumentException("Track is already occupied");
        });
    //checks if the train departure exists in the list of train departures
    trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst()
        //if it exists it sets the new track for the existing train departure
        .ifPresent(trainDeparture -> trainDeparture.setTrack(track));
    //if the train departure does not exist it throws an exception
    if (trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        .findFirst().isEmpty()) {
      throw new IllegalArgumentException("The train departure does not exist");
    }
  }

  /**
   * Uses streams to find a specific train departure in the already existing list of train
   * departures. and shows all the attributes of that exact train departure. does this only after
   * verifying that the train number is not negative and that the train departure exists in the
   * list of train departures. uses the toString method of the TrainDeparture class:
   * :{@link TrainDeparture#toString()}
   * verifies user input using the method:
   * :{@link #verifyIntegerFormat(String, String)}
   *
   * @param trainNumberString is a String that is parsed to an Integer in the method and is the
   *                          specific train number for the train departure the user searches for.
   * @return a String with the train departure and all of its attributes for the user to see,
   *         for example the departure time and line.
   * @throws IllegalArgumentException if the train number does not have a corresponding
   *                                  train departure.
   */

  public String findTrainDepartureUsingTrainNumber(String trainNumberString) {
    //verifies that the input is in the correct format
    verifyIntegerFormat(trainNumberString, "train number");
    int trainNumber = Integer.parseInt(trainNumberString);
    //creates a string with the train departure and all of its attributes
    String output;
    output = trainDepartures.stream()
        //checks if the train departure exists in the list of train departures
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == (trainNumber))
        //uses the toString method of the TrainDeparture class
        .map(TrainDeparture::toString)
        //then reduces it to a single string in the output variable
        .reduce("", (a, b) -> a + b);
    //if the train departure does not exist it throws an exception
    if (output.isBlank()) {
      throw new IllegalArgumentException("There is no train departure with that train number");
    }
    return output;
  }

  /**
   * Uses streams to go through the existing list of train departures to find all the
   * train departures going to a destination that the user puts in. does this only after
   * verifying that the destination is not blank and that the destination exists in the list
   *
   * @param destination Is a String that is used to specify what destination the train goes to
   *                    and the method checks if there are any train departures going there.
   * @return a String with all the train departures going to the destination the user specified.
   * @throws IllegalArgumentException if the destination is blank or if there are no train
   *                                  departures going to the destination.
   */

  public String findAllDeparturesToDestination(String destination) {
    //checks if the destination-input is blank
    if (destination.isBlank()) {
      throw new IllegalArgumentException("Destination cannot be blank");
    }
    //creates a string with all the train departures going to the destination
    String output;
    output = trainDepartures.stream()
        //filters and leaves in all the train departures that are going to the destination
        .filter(trainDeparture -> trainDeparture.getDestination().equals(destination))
        .map(TrainDeparture::toString)
        .reduce("", (a, b) -> a + b);
    //if there are no train departures going to the destination it throws an exception
    if (output.isBlank()) {
      throw new IllegalArgumentException("There are no train departures going to that destination");
    }
    return output;
  }

  /**
   * Uses the removeIf method to remove all the train departures that have already departed. by
   * comparing the departure time plus the delay of the train departure to the station clock. Does
   * this using the isBefore method of the LocalTime class.
   */
  public void updateTrainDepartures() {
    //removes all the train departures that have already departed
    trainDepartures.removeIf(trainDeparture -> trainDeparture.addDelayToDepartureTime()
        .isBefore(stationClock));
  }

  /**
   * Uses streams to sort the list of train departures by departure time and delay. does this
   * by comparing the departure time plus the delay and by use of the comparator class,
   * and the comparing method, it sorts it in ascending order. does this only after verifying
   * that there are train departures left in the list.
   */

  //Used copilot to make the method with streams and a comparator.
  public void sortByDepartureTimeAndDelay() {
    //sorts the list of train departures by departure time and delay using streams and comparator
    trainDepartures.sort(Comparator.comparing(TrainDeparture::addDelayToDepartureTime));
  }

  /**
   * Uses streams to find all the train departures in the already existing list of train
   * departures in a specific interval. does this by comparing the departure time plus the delay
   * of the train departure to the start and end time of the interval. using method:
   * :{@link  TrainDepartureRegister#sortByDepartureTimeAndDelay()}
   * does this only after verifying that the start time is before the end time. and also verifying
   * other inputs from user using the method:
   * :{@link #verifyLocalTimeFormat(String, String)}
   *
   * @param startTime is a String that gets parsed to the LocalTime class and represents the
   *                  start time of the interval.
   * @param endTime   is a String that gets parsed to the LocalTime class and represents the
   *                  end time of the interval.
   * @return a String with all the train departures in the interval.
   * @throws IllegalArgumentException if the start time is after the end time or if there are
   *                                  no train departures in the interval.
   */
  public String findTrainDeparturesInInterval(String startTime, String endTime) {
    //verifies that the input is in the correct format
    verifyLocalTimeFormat(startTime, "start time");
    verifyLocalTimeFormat(endTime, "end time");
    LocalTime start = LocalTime.parse(startTime);
    LocalTime end = LocalTime.parse(endTime);
    //sorts the list of train departures by departure time and delay
    sortByDepartureTimeAndDelay();
    //checks if the start time is after the end time, if it is it throws an exception
    if (end.isBefore(start)) {
      throw new IllegalArgumentException("the last input has to be after the first");
    }
    //creates a string with all the train departures in the interval
    String output;
    output = trainDepartures.stream()
        //filters and leaves in all the train departures that are in the interval
        .filter(trainDeparture -> trainDeparture.addDelayToDepartureTime().isAfter(start))
        .filter(trainDeparture -> trainDeparture.addDelayToDepartureTime().isBefore(end))
        .map(TrainDeparture::toString)
        .reduce("", (a, b) -> a + b);
    if (output.isBlank()) {
      throw new IllegalArgumentException("There are no train departures in that interval");
    }
    return output;
  }

  /**
   * Uses streams to show all the train departures in the already existing list of train
   * departures. if there are no train departures left in the list it throws an exception.
   * does this by using the toString method of the TrainDeparture class:
   * :{@link TrainDeparture#toString()}
   *
   * @return a String with all the train departures in the list.
   */
  public String showAllTrainDepartures() {
    //first sorts the list of train departures by departure time and delay
    sortByDepartureTimeAndDelay();
    //creates a string with all the train departures in the list
    String output;
    output = trainDepartures.stream()
        .map(TrainDeparture::toString)
        .reduce("", (a, b) -> a + b);
    //if there are no train departures left in the list it throws an exception
    if (output.isBlank()) {
      throw new IllegalArgumentException("There are no train departures left today");
    }
    return output;
  }

  //-----------Verification methods-----------------

  /**
   * Method to check if inputs from user are in the correct format of the LocalTime class, if the
   * user has not written in a correct format for the method to parse, the test fails. if the
   * verifying fails it throws an exception.
   *
   * @param parameter     Is the String that is being verified if it is parsable to LocalTime class.
   * @param parameterName Name of the String being verified, this gets utilized when the exception
   *                      is thrown, to know what String failed.
   * @throws IllegalArgumentException Throws IllegalArgumentException if input is not in the
   *                                  correct format. for example a word instead of a time in
   *                                  "hh:mm" format. It is then considered illegal and is thrown
   *                                  as an exception.
   */
  public void verifyLocalTimeFormat(String parameter, String parameterName) {
    //checks if the input is in the correct format of a LocalTime
    try {
      LocalTime.parse(parameter);
    } catch (Exception e) {
      //if it is not in the correct format it throws an exception
      throw new IllegalArgumentException(parameterName + "has to be in the format HH:MM");
    }
  }

  /**
   * Method to check if inputs from user are in the correct format for the Integer class, if the
   * user has not written in a correct format for the method to parse, the test fails. and if the
   * input is negative or 0 it also fails. if the verifying fails it throws an exception.
   *
   * @param parameter     Is the String that is being verified, if it is parsable to Integer class.
   * @param parameterName Name of the String being verified, this gets utilized when the exception
   *                      is thrown, to know what String failed.
   * @throws IllegalArgumentException Throws IllegalArgumentException if input is not in the
   *                                  correct format. for example a word instead of a number in
   *                                  "123" format. It is then considered illegal and is thrown
   *                                  as an exception. also throws an exception if the input is
   *                                  negative or 0.
   */

  public void verifyIntegerFormat(String parameter, String parameterName) {
    //checks if the input is in the correct format of an Integer
    try {
      Integer.parseInt(parameter);
      //then checks if the input is negative or 0, if it is it throws an exception
      if (Integer.parseInt(parameter) <= 0) {
        throw new IllegalArgumentException();
      }
    } catch (IllegalArgumentException e) {
      //if it is not in the correct format it throws an exception
      throw new IllegalArgumentException(parameterName + " has to be a positive number");
    }
  }
}

