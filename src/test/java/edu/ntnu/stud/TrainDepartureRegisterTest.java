package edu.ntnu.stud;
import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class TrainDepartureRegisterTest {

  private static TrainDepartureRegister trainDepartureRegister;
  @BeforeEach
  public void init(){
    trainDepartureRegister = new TrainDepartureRegister();
  }
  @Nested
  @DisplayName("Positive train departure register tests")
  public class PositiveTrainDepartureRegisterTests {
    @Test
    @DisplayName("isAbleToGetClock")
    public void isAbleToGetClock() {
      trainDepartureRegister.setStationClock("12:00");
      assertEquals("12:00", trainDepartureRegister.getStationClock());
    }
    @Test
    @DisplayName("isAbleToGetStationClock")
    public void isAbleToGetStationClock() {
      assertEquals("05:00", trainDepartureRegister.getStationClock());
    }

    @Test
    @DisplayName("isAbleToAddATrainDeparture")
    public void isAbleToAddATrainDeparture() {
      assertEquals(0, trainDepartureRegister.getSize());
      trainDepartureRegister.addTrainDeparture("12:00","L1","12","Bodø", "00:00");
      assertEquals(1, trainDepartureRegister.getSize());
    }
    @Test
    @DisplayName("isableToDeleteATrainDeparture")
    public void isAbleToDeleteATrainDeparture(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","12","Bodø", "00:00");
      assertEquals(1,trainDepartureRegister.getSize());
      trainDepartureRegister.deleteTrainDeparture("12");
      assertEquals(0,trainDepartureRegister.getSize());
    }
    @Test
    @DisplayName("isAbleToSetDelay")
    public void isAbleToSetDelay(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","57","Trondheim","00:00");
      assertDoesNotThrow(() -> trainDepartureRegister.setDelayForTrainDeparture("57","00:30"));
    }
    @Test
    @DisplayName("isAbleToSetTrack")
    public void isAbleToSetTrack(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","57","Trondheim","00:00");
      assertDoesNotThrow(() -> trainDepartureRegister.setTrackForTrainDeparture("57","1"));
    }
    @Test
    @DisplayName("isAbleToFindTrainNumberOfTrainDeparture")
    public void isAbleToFindTrainNumberOfTrainDeparture(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","57","Trondheim","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","58","Trondheim","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","60","Oslo","00:00");
      trainDepartureRegister.addTrainDeparture("11:00","L2","63","Bergen","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","59","Trondheim","00:00");
      assertDoesNotThrow(() -> trainDepartureRegister.findTrainDepartureUsingTrainNumber("60"));
    }
    @Test
    @DisplayName("isAbleToFindAllDeparturesToDestination")
    public void isAbleToFindAllDeparturesToDestination(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","57","Trondheim","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","58","Trondheim","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","60","Oslo","00:00");
      trainDepartureRegister.addTrainDeparture("11:00","L2","63","Bergen","00:00");
      trainDepartureRegister.addTrainDeparture("12:00","L1","59","Trondheim","00:00");
      assertDoesNotThrow(() -> trainDepartureRegister.findAllDeparturesToDestination("Trondheim"));
    }
    @Test
    @DisplayName("isAbleToUpdateTrainDepartureRegister")
    public void isAbleToUpdateTrainDepartureRegister() {
      trainDepartureRegister.addTrainDeparture("11:00","L2","63","Oslo", "00:00");
      assertEquals(1, trainDepartureRegister.getSize());
      trainDepartureRegister.setStationClock("13:00");
      trainDepartureRegister.updateTrainDepartures();
      assertEquals(0, trainDepartureRegister.getSize());
    }
    @Test
    @DisplayName("isAbleToSortTrainDepartures")
    public void isAbleToSortTrainDepartures(){
      trainDepartureRegister.addTrainDeparture("16:00","F2", "40", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("12:00","F3", "41", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("11:00","F4", "42", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("14:00","F5", "43", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("15:00","F6", "44", "Trondheim", "00:00");
      trainDepartureRegister.sortByDepartureTimeAndDelay();
      assertEquals(42, trainDepartureRegister.getTrainDeparture(0).getTrainNumber());
      assertEquals(41, trainDepartureRegister.getTrainDeparture(1).getTrainNumber());
      assertEquals(43, trainDepartureRegister.getTrainDeparture(2).getTrainNumber());
      assertEquals(44, trainDepartureRegister.getTrainDeparture(3).getTrainNumber());
      assertEquals(40, trainDepartureRegister.getTrainDeparture(4).getTrainNumber());
    }
    @Test
    @DisplayName("isAbleToFindTrainDeparturesInInterval")
    public void isAbleToFindTrainDeparturesInInterval(){
      trainDepartureRegister.addTrainDeparture("16:00","L1", "37", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("12:00","F3", "49", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("10:00","S7", "45", "Trondheim", "01:00");
      trainDepartureRegister.addTrainDeparture("14:00","F5", "43", "Trondheim", "00:00");
      trainDepartureRegister.addTrainDeparture("15:00","F6", "90", "Trondheim", "00:00");
      trainDepartureRegister.sortByDepartureTimeAndDelay();
      assertDoesNotThrow(() -> trainDepartureRegister.findTrainDeparturesInInterval("11:00","14:00"));
    }
    @Test
    @DisplayName("passesIfTheLocalTimeFormatIsCorrect")
    public void passesIfTheLocalTimeFormatIsCorrect(){
      assertDoesNotThrow(() -> trainDepartureRegister.verifyLocalTimeFormat("12:00","Delay"));
    }
    @Test
    @DisplayName("passesIfIntegerIsPositive")
    public void passesIfIntegerIsPositive(){
      assertDoesNotThrow(() -> trainDepartureRegister.verifyIntegerFormat("1","Train number"));
    }
  }
  @Nested
  @DisplayName("Negative train departure register tests")
  public class NegativeTrainDepartureRegisterTests {

    @Test
    @DisplayName("throwExceptionIfStationClockIsBlank")
    public void throwExceptionIfStationClockIsBlank(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.setStationClock(""));
    }
    @Test
    @DisplayName("throwsExceptionIfStationClockInputIsBeforeOriginal")
    public void throwsExceptionIfStationClockInputIsBeforeOriginal(){
      trainDepartureRegister.setStationClock("07:00");
      assertThrows(IllegalArgumentException.class,()-> trainDepartureRegister.setStationClock("06:00"));
    }
    @Test
    @DisplayName("ThrowsExceptionIfTrainDepartureAlreadyExists")
    public void throwsExceptionIfTrainDepartureAlreadyExists(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","12","Bodø", "00:00");
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.addTrainDeparture("12:00","L1","12","Bodø", "00:00"));
    }
    @Test
    @DisplayName("throwsExceptionIfTrainDepartureAlreadyExists2")
    public void throwsExceptionIfTrainDepartureAlreadyExists2(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","12","Bodø", "00:00");
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.addTrainDeparture("11:00","F1","12","Bergen", "00:00"));
    }
    @Test
    @DisplayName("throwsExceptionIfTrainDepartureDoesNotExist")
    public void throwsExceptionIfTrainDepartureDoesNotExist(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.deleteTrainDeparture("12"));
    }
    @Test
    @DisplayName("throwsExceptionIfLocalTimeFormatIsWrongInDepartureTime")
    public void throwsExceptionIfLocalTimeFormatIsWrong(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.addTrainDeparture("Fish","L1","12","Bodø", "00:00"));
    }
    @Test
    @DisplayName("throwsExceptionIfLocalTimeFormatIsWrongInDelay")
    public void throwsExceptionIfLocalTimeFormatIsWrongInDelay(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.addTrainDeparture("12:00", "L2", "60","Trondheim","fish"));
    }
    @Test
    @DisplayName("throwsIfTheTrainDepartureDoesNotExistSetDelay")
    public void throwsIfTheTrainDepartureDoesNotExistSetdelay(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.setDelayForTrainDeparture("80","00:11"));
    }
    @Test
    @DisplayName("throwsIfTheTrainDepartureDoesNotExistSetTrack")
    public void throwsIfTheTrainDepartureDoesNotExistSetTrack(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.setTrackForTrainDeparture("80","3"));
    }
    @Test
    @DisplayName("throwsIfTheTrackIsAboveMax")
    public void throwsIfTheTrackIsAboveMax(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","80","Bodø", "00:00");
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.setTrackForTrainDeparture("80","9"));
    }
    @Test
    @DisplayName("throwsIfTheTrackIsBelowMin")
    public void throwsIfTheTrackIsBelowMin(){
      trainDepartureRegister.addTrainDeparture("12:00","L1","80","Bodø", "00:00");
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.setTrackForTrainDeparture("80","-1"));
    }
    @Test
    @DisplayName("throwsIfTrackIsOccupied")
    public void throwsIfTrackIsOccupied(){
      trainDepartureRegister.addTrainDeparture("11:00","F3", "42","Trondhiem","00:00");
      trainDepartureRegister.setTrackForTrainDeparture("42","1");
      trainDepartureRegister.addTrainDeparture("08:00","F2", "60", "Bergen", "00:00");
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.setTrackForTrainDeparture("60","1"));
    }
    @Test
    @DisplayName("throwsExceptionIfTrainNumberIsNegative")
    public void throwsExceptionIfTrainNumberIsNegative(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.findTrainDepartureUsingTrainNumber("-1"));
    }
    @Test
    @DisplayName("throwsExceptionIfThereIsNoTrainDepartureWithGivenTrainNumber")
    public void throwsExceptionIfThereIsNoTrainDepartureWithGivenTrainNumber(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.findTrainDepartureUsingTrainNumber("1"));
    }
    @Test
    @DisplayName("throwsExceptionIfDestinationIsBlank")
    public void throwsExceptionIfDestinationIsBlank(){
      assertThrows(IllegalArgumentException.class, ()  -> trainDepartureRegister.findAllDeparturesToDestination(""));
    }
    @Test
    @DisplayName("throwsExceptionIfDestinationDoesNotExist")
    public void throwsExceptionIfDestinationDoesNotExist(){
    assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.findAllDeparturesToDestination("Bergen"));
    }
    @Test
    @DisplayName("throwsExceptionIfThereAreNoTrainDeparturesInInterval")
    public void throwsExcepionIfThereAreNoTrainDeparturesInInterval(){
      assertThrows(IllegalArgumentException.class, ()->trainDepartureRegister.findTrainDeparturesInInterval("12:00","16:00"));
    }
    @Test
    @DisplayName("throwsifEndTimeIsBeforeStartTime")
    public void throwsifEndTimeIsBeforeStartTime(){
      assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.findTrainDeparturesInInterval("12:00","06:00"));
    }
    @Test
    @DisplayName("throwsExceptionIfArrayListIsEpmty")
    public void throwsExceptionIfArrayListIsEpmty(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.showAllTrainDepartures());
    }
    @Test
    @DisplayName("throwsIfTheLocalTimeInputIsInWrongFormat")
    public void throwsIfTheLocalTimeInputIsInWrongFormat(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.verifyLocalTimeFormat("fish","Delay"));
    }
    @Test
    @DisplayName("throwsIfIntegerIsNegative")
    public void throwsIfIntegerIsNegative(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.verifyLocalTimeFormat("-1","track"));
    }
    @Test
    @DisplayName("throwsIfIntegerIsZero")
    public void throwsIfIntegerIsZero(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.verifyLocalTimeFormat("0","Train number"));
    }
    @Test
    @DisplayName("throwsIfIntegerIsInWrongFormat")
    public void throwsIfIntegerIsInWrongFormat(){
      assertThrows(IllegalArgumentException.class,() -> trainDepartureRegister.verifyLocalTimeFormat("fish","Train number"));
    }
  }
}
