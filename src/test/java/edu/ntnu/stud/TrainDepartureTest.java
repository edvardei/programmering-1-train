package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

public class TrainDepartureTest {

  @Nested
  @DisplayName("Positive train departure tests")
  public class PositiveTrainDepartureTests {
    @Test
    @DisplayName("isAbleToPrintAnObjectOfTrainDeparture")
    public void isAbleToPrintAnObjectOfTrainDeparture() {
      TrainDeparture traindeparture = new TrainDeparture
          ("11:00", "L1", "55", "Bodø", "1", "00:00");
      assertEquals("11:00", traindeparture.getDepartureTime().toString());
      assertEquals("L1", traindeparture.getLine());
      assertEquals(55, traindeparture.getTrainNumber());
      assertEquals("Bodø", traindeparture.getDestination());
      assertEquals(1, traindeparture.getTrack());
      assertEquals(LocalTime.parse("00:00"), traindeparture.getDelay());
    }

    @Test
    @DisplayName("isAbleToSetDelay")
    public void isAbleToSetDelay() {
      TrainDeparture traindeparture = new TrainDeparture
          ("11:00", "L1", "55", "Bodø", "1", "00:00");
      assertEquals("00:00", traindeparture.getDelay().toString());
      traindeparture.setDelay("00:30");
      assertEquals("00:30", traindeparture.getDelay().toString());
    }

    @Test
    @DisplayName("isAbleToSetTrack")
    public void isAbleToSetTrack() {
      TrainDeparture traindeparture = new TrainDeparture
          ("11:00", "L1", "55", "Bodø", "1", "00:00");
      assertEquals(1, traindeparture.getTrack());
      traindeparture.setTrack(2);
      assertEquals(2, traindeparture.getTrack());
    }

    @Test
    @DisplayName("isAbleToAddDelayToDepartureTime")
    public void isAbleToAddDelayToDepartureTime() {
      TrainDeparture traindeparture = new TrainDeparture
          ("11:00", "L1", "55", "Bodø", "1", "00:00");
      assertEquals("11:00", traindeparture.getDepartureTime().toString());
      traindeparture.setDelay("00:30");
      assertEquals("11:30", traindeparture.addDelayToDepartureTime().toString());
    }
  }

  @Nested
  @DisplayName("Negative train departure tests")
  public class NegativeTrainDepartureTests {
    @Test
    @DisplayName("throwsExceptionIfTrackIsZero")
    public void throwsExceptionIfTrackIsBlank() {
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture
              ("11:00", "L1", "55", "Bodø", "", "00:00"));
    }

    @Test
    @DisplayName("throwsExceptionIfDepartureTimeIsBlank")
    public void throwsExceptionIfDepartureTimeIsBlank() {
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture
              ("", "L1", "55", "Bodø", "1", "00:00")
      );
    }

    @Test
    @DisplayName("throwsExceptionIfLineIsBlank")
    public void throwsExceptionIfLineIsBlank() {
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture
              ("11:00", "", "55", "Bodø", "1", "00:00")
      );
    }

    @Test
    @DisplayName("throwsExceptionIfTrainNumberIsBlank")
    public void throwsExceptionIfTrainNumberIsNegative() {
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture
              ("11:00", "L1", "", "Bodø", "1", "00:00")
      );
    }

    @Test
    @DisplayName("throwsExceptionIfDestinationIsBlank")
    public void throwsExceptionIfDestinationIsBlank() {
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture
              ("11:00", "L1", "55", "", "1", "00:00")
      );
    }
  }
}