package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class UserInterfaceTest {
  private static UserInterface ui;

  @BeforeAll
  public static void init() {
    ui = new UserInterface();
  }

  @Nested
  @DisplayName("Positive user interface tests")
  public class PositiveUserInterfaceTests {
    @Test
    @DisplayName("isAbleToVerifyChoice")
    public void isAbleToVerifyChoice() {
      assertDoesNotThrow(() -> ui.verifyChoice("0"));
      assertDoesNotThrow(() -> ui.verifyChoice("1"));
      assertDoesNotThrow(() -> ui.verifyChoice("2"));
      assertDoesNotThrow(() -> ui.verifyChoice("3"));
      assertDoesNotThrow(() -> ui.verifyChoice("4"));
      assertDoesNotThrow(() -> ui.verifyChoice("5"));
      assertDoesNotThrow(() -> ui.verifyChoice("6"));
      assertDoesNotThrow(() -> ui.verifyChoice("7"));
      assertDoesNotThrow(() -> ui.verifyChoice("8"));
      assertDoesNotThrow(() -> ui.verifyChoice("9"));
    }
  }

  @Nested
  @DisplayName("Negative user interface tests")
  public class NegativeUserInterfaceTests {
    @Test
    @DisplayName("throwsIllegalArgumentExceptionWhenChoiceIsNotBetween0And8")
    public void throwsIllegalArgumentExceptionWhenChoiceIsNotBetween0And8(){
      assertThrows(IllegalArgumentException.class, () -> ui.verifyChoice("-1"));
      assertThrows(IllegalArgumentException.class, () -> ui.verifyChoice("10"));
    }
  }
}
